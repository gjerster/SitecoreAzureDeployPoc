$thumbprint = (New-SelfSignedCertificate `
    -Subject "CN=Sitecore Azure POC" `
    -Type SSLServerAuthentication `
    -FriendlyName "SitecoreAzurePoc Certificate").Thumbprint


$certificateFilePath = "$PSScriptRoot\$thumbprint.pfx"
Export-PfxCertificate `
    -cert cert:\LocalMachine\MY\$thumbprint `
    -FilePath "$certificateFilePath" `
    -Password (Read-Host -Prompt "Enter password that would protect the certificate" -AsSecureString)