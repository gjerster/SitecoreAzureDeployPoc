$SCSDK="C:\Tools\Sitecore Azure Toolkit 2.0.2 rev. 180220"
$SCTemplates="https://gitlab.com/gjerster/SitecoreAzureDeployPoc/raw/master/AzureXPScaled/Sitecore%209.0.1/XP"
$DeploymentId = "SitecoreAzureDeployPocXPScaled"
$LicenseFile = "$PSScriptRoot\..\license.xml"
$CertificateFile = "$PSScriptRoot\certificate.pfx"

# $SubscriptionId = "7dd6b5f5-2152-49b1-b313-8120d1548013"
$Location="northeurope"
$ParamFile="$PSScriptRoot\azuredeploy.parameters.json"
$Parameters = @{
    "deploymentId"=$DeploymentId;
    "authCertificateBlob" = [System.Convert]::ToBase64String([System.IO.File]::ReadAllBytes($CertificateFile))

}
Import-Module $SCSDK\tools\Sitecore.Cloud.Cmdlets.psm1
# Add-AzureRMAccount
# Set-AzureRMContext -SubscriptionId $SubscriptionId
Start-SitecoreAzureDeployment -Name $DeploymentId -Location $Location -ArmTemplateUrl "$SCTemplates/azuredeploy.json"  -ArmParametersPath $ParamFile  -LicenseXmlPath $LicenseFile  -SetKeyValue $Parameters